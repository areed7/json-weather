import acm.program.*;
import acm.gui.*;
import javax.swing.*;
import java.awt.Dimension;
import java.awt.event.*;
import java.awt.Color;


public class WeatherGUI extends Program
{
    private JTextField zipCode, temperature, location, weather, windSpeed;
    private JLabel zipIn, tempOut, locationOut, weatherOut, windSpeedOut;
    private JButton goButton, clearButton;
    
    
    public WeatherGUI()
    {
        start();
        setSize(400, 200);
        setBackground(Color.CYAN);
        
        
    }

    /* TODO: Add a function that changes the background of the application based on the weather.
    public void backgroundWeather(String weather) {


        switch (weather) {
            case "Clear":
                setBackground(Color.CYAN);
                break;
        }
    }*/
    
    public void init()
    {
        TableLayout table = new TableLayout(6, 2);
        setLayout(table);
        
        zipCode = new JTextField();
        location = new JTextField();
        weather = new JTextField();
        temperature = new JTextField();
        windSpeed = new JTextField();
        
        
        temperature.setEditable(false);
        location.setEditable(false);
        weather.setEditable(false);
        windSpeed.setEditable(false);
        
        
        Dimension d = zipCode.getPreferredSize();
        d.setSize(200, d.getHeight());
        zipCode.setPreferredSize(d);
        
        zipIn = new JLabel("<html><b>ZipCode</b></html>");
        zipIn.setForeground(Color.BLACK);

        locationOut = new JLabel("<HTML><b>LOCATION: </b></html>");
        locationOut.setForeground(Color.BLACK);

        weatherOut = new JLabel("<HTML><b>WEATHER: </b></HTML>");
        weatherOut.setForeground(Color.BLACK);

        windSpeedOut = new JLabel("<HTML><b>WIND SPEED: </b></HTML>");
        windSpeedOut.setForeground(Color.BLACK);

        tempOut = new JLabel("<html><b>TEMPERATURE: </b></html>");
        tempOut.setForeground(Color.BLACK);
        
        goButton = new JButton("Go!");
        goButton.setActionCommand("Go!");
        clearButton = new JButton("Clear");
        clearButton.setActionCommand("Clear");
        
        add(zipIn);
        add(zipCode);
        add(locationOut);
        add(location);
        add(weatherOut);
        add(weather);
        add(windSpeedOut);
        add(windSpeed);
        add(tempOut);
        add(temperature);
        add(goButton);
        add(clearButton);
        
        
        
        addActionListeners();
    }
    
    public void actionPerformed(ActionEvent ae)
    {
        String zip = ""; // Change this line
        String postfixString;
        WeatherJSON weatherJSON;
        
        String what = ae.getActionCommand();
        if (what.equals("Clear"))
        {
            zipCode.setText("");
            temperature.setText("");
            location.setText("");
            windSpeed.setText("");
            weather.setText("");
            
        }
        else if (what.equals("Go!"))
        {
            zip = zipCode.getText();
            weatherJSON = new WeatherJSON(Integer.parseInt(zip));
            //urlShort = urlShortner.getShortURL();
            location.setText("" + weatherJSON.getCity());
            weather.setText("" + weatherJSON.getWeather());
            temperature.setText("" + weatherJSON.getTemperature());
            windSpeed.setText("" + weatherJSON.getWindSpeed());


        }
    }


    public static void main(String[] args){

        new WeatherGUI();

    }
}