import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.net.URL;

import com.google.gson.*;

public class WeatherJSON
{
	// WeatherJSON OAuth access token (a unique ID).
	// You can register for your own access token, or use the one here.
	//KEY 56fbbd7d5825e326
	//http://api.wunderground.com/api/56fbbd7d5825e326/conditions/q/95677.json
	private final String ACCESS_TOKEN = "56fbbd7d5825e326";

	private JsonElement jse;
	
	public WeatherJSON(int zipCode)
	{
		jse = null;

		try
		{
			// Encode given URL -- could throw UnsupportedEncodingException
			//String encodedURL = URLEncoder.encode(userURL, "utf-8");

			// Construct WeatherJSON API URL
			URL weatherJSON = new URL("http://api.wunderground.com/api/" +
					ACCESS_TOKEN +
					"/conditions/q/" +
					zipCode + ".json");
			// Open the URL
			InputStream is = weatherJSON.openStream(); // throws an IOException
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			// Read the result into a JSON Element
			jse = new JsonParser().parse(br);

			// Close the connection
			is.close();
			br.close();
		}
		catch (java.io.UnsupportedEncodingException uee)
		{
			uee.printStackTrace();
		}
		catch (java.net.MalformedURLException mue)
		{
			mue.printStackTrace();
		}
		catch (java.io.IOException ioe)
		{
			ioe.printStackTrace();
		}

		if (jse != null)
		{
			System.out.println(jse.toString());


		}
	}

	public double getTemperature() {

		 return Double.parseDouble(jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString());

	}

	public double getWindSpeed(){
		return Double.parseDouble(jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_mph").getAsString());
	}

	public String getCity(){
		return jse.getAsJsonObject().get("current_observation").
				   getAsJsonObject().get("display_location").
				   getAsJsonObject().get("full").getAsString();
	}

	public String getWeather(){
		return jse.getAsJsonObject().get("current_observation").
				   getAsJsonObject().get("weather").getAsString();
	}


	/* USE AS EXAMPLE
	public String getShortURL()
	{
		// Extract shortened URL from JSON string.
		// { "data": { ... "url" : "http://bit.ly/AbCdEF" ... } }
		String shortURL = jse.getAsJsonObject().get("data")
		                     .getAsJsonObject().get("url").getAsString();
		return shortURL;
	}*/

	/* USE AS EXAMPLE
	public String getHash()
	{
		return jse.getAsJsonObject().get("data")
                .getAsJsonObject().get("hash").getAsString();
	}*/


	public static void main(String[] args) {
		WeatherJSON b = new WeatherJSON(95677);

		System.out.println("TEMP: " + b.getTemperature());

	}
}